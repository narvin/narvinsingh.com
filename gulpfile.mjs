/* eslint-disable import/no-extraneous-dependencies */
import { promises as fsPromises } from "fs";
import through from "through2";
import gulp from "gulp";
import del from "del";
import ren from "gulp-rename";
import htmlmin from "gulp-htmlmin";
import sourcemaps from "gulp-sourcemaps";
import rev from "gulp-rev";
import revRewrite from "gulp-rev-rewrite";
import sass from "gulp-dart-sass";
import postcss from "gulp-postcss";
import autoprefixer from "autoprefixer";
import cssnano from "cssnano";
import { rollup } from "rollup";
import uglify from "gulp-uglify";

const { access, readFile } = fsPromises;
const { src, dest, series, parallel } = gulp;

const dir = {
  src: "src", dest: "dist", asset: "asset", style: "style", script: "script",
};
const filesToCopy = [`${dir.src}/.htaccess`, `${dir.src}/favicon.ico`];
const htmlPath = `${dir.src}/**/*.html`;
const scssFile = `${dir.src}/${dir.asset}/${dir.style}/main.scss`;
const mjsFile = `${dir.src}/${dir.asset}/${dir.script}/main.mjs`;
const jsFile = `${dir.dest}/${dir.asset}/${dir.script}/main.js`;

function clean() {
  return del([dir.dest]);
}

function copyMisc() {
  return src(filesToCopy).pipe(dest(dir.dest));
}

function buildDevHtml() {
  return src(htmlPath).pipe(dest(dir.dest));
}

function buildHtml() {
  return src(htmlPath)
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest(dir.dest));
}

function buildDevCss() {
  return src(scssFile)
    .pipe(sass())
    .pipe(postcss([autoprefixer]))
    .pipe(dest(`${dir.dest}/${dir.asset}/${dir.style}`));
}

function buildCss() {
  return src(scssFile)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss([autoprefixer, cssnano]))
    .pipe(ren({ suffix: ".min" }))
    .pipe(rev())
    .pipe(sourcemaps.write("."))
    .pipe(dest(`${dir.dest}/${dir.asset}/${dir.style}`))
    .pipe(rev.manifest("rev-css-manifest.json"))
    .pipe(dest(`${dir.dest}/${dir.asset}`));
}

let isJsRolledUp = false;

function rollupJs() {
  return access(mjsFile)
    .then(() => rollup({ input: mjsFile }))
    .then((bundle) => bundle.write({
      file: jsFile,
      format: "iife",
      sourcemap: false,
    })).then(() => {
      isJsRolledUp = true;
    })
    .catch((e) => console.log(e)); // eslint-disable-line no-console
}

function buildDevJs() {
  if (!isJsRolledUp) return Promise.resolve();

  return src(jsFile).pipe(dest(`${dir.dest}/${dir.asset}/${dir.script}`));
}

function buildJs() {
  if (!isJsRolledUp) return Promise.resolve();

  return src(jsFile)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(ren({ suffix: ".min" }))
    .pipe(rev())
    .pipe(sourcemaps.write("."))
    .pipe(dest(`${dir.dest}/${dir.asset}/${dir.script}`))
    .pipe(rev.manifest("rev-js-manifest.json"))
    .pipe(dest(`${dir.dest}/${dir.asset}`));
}

function delJs() {
  return del([jsFile]);
}

function reviseDev() {
  return src(htmlPath)
    .pipe(through.obj((file, _, cb) => {
      if (file.isBuffer()) {
        const buf = file;
        buf.contents = Buffer.from(buf.contents.toString()
          .replace(/(\shref="[^"]*)\.min\.css"/g, "$1.css\"")
          .replace(/(\ssrc="[^"]*)\.min\.js"/g, "$1.js\""));
        cb(null, buf);
      } else cb(null, file);
    }))
    .pipe(dest(dir.dest));
}

function revise(type) {
  return readFile(`${dir.dest}/${dir.asset}/rev-${type}-manifest.json`)
    .then((manifest) => {
      src(`${dir.dest}/**/*.html`)
        .pipe(revRewrite({ manifest }))
        .pipe(dest(dir.dest));
    }).catch((e) => console.log(e)); // eslint-disable-line no-console
}

function reviseCss() {
  return revise("css");
}

function reviseJs() {
  return revise("js");
}

const dev = series(
  parallel(
    copyMisc,
    buildDevHtml,
    buildDevCss,
    series(rollupJs, buildDevJs),
  ),
  reviseDev,
);
const build = series(
  clean,
  parallel(
    copyMisc,
    buildHtml,
    buildCss,
    series(rollupJs, buildJs, delJs),
  ),
  reviseCss, reviseJs,
);
export { dev as default, build, clean };

