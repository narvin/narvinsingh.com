import greet from "./greet";
import { env, css, js, id } from "./page-info";

greet();

window.addEventListener("load", () => {
  const targetEnv = "dev";

  if (env() === targetEnv) {
    const hrefCss = css();
    const hrefJs = js();
    const idCss = id(hrefCss);
    const idJs = id(hrefJs);
    const pCss = document.createElement("p");
    const pJs = document.createElement("p");

    pCss.innerHTML = `CSS: ${idCss}`;
    pJs.innerHTML = `JS: ${idJs}`;
    document.body.appendChild(pCss);
    document.body.appendChild(pJs);
  }
});

